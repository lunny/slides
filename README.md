# Slides of lunny

You have to install golang at first.

```
go get golang.org/x/tools/cmd/present
go install golang.org/x/tools/cmd/present
git clone https://gitea.com/lunny/slides
cd slides && present ./
```

Then visit `http://localhost:3999` from your web browser.